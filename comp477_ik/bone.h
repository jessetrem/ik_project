#pragma once
#include <glm/glm.hpp>
#include "model.h"
#include "shader.h"

class Renderer;

struct Joint
{
  Joint();
  Joint(glm::vec3 position, bool constrained = true, float upRestriction = 0.8f, float downRestriction = 0.8f, float leftRestriction = 0.8f, float rightRestriction = 0.8f);
  ~Joint();
  glm::vec3 position;
  glm::vec3 constraintDirection;
  float up;
  float down;
  float left;
  float right;
  bool constrained;
};

class Bone
{
public:
  static void Init();
  static void Destroy();
  Bone();
  Bone(float length, glm::vec3 direction, glm::vec3 position);
  Bone(Joint* start, Joint* end);
  ~Bone();
  void AdjustForward();
  void AdjustConstraintsForward();
  void AdjustBackward();
  void AdjustConstraintsBackward();
  void ComputeModelTransforms();
  void Draw(Renderer* renderer);
private:
  float m_length;
  glm::vec3 m_direction;
  glm::vec3 m_position;
  Joint* m_start;
  Joint* m_end;

  glm::mat4 f_getDirectionalRotation();

  static Model* MODEL;
  static Shader* SHADER;
  static glm::vec3 BONE_COLOR;
  static BasicVertex BONE_VERTICIES[6];
  static unsigned int BONE_INDICIES[24];
};

