#pragma once
#include <glm/glm.hpp>
#include <vector>
class Camera;
class Cursor;
struct AABB;

struct Intersectable
{
  AABB* boundingBox;
};

class RayCaster
{
public:
  RayCaster();
  ~RayCaster();

  void RegisterBoundingBox(AABB* bondingBox);
  AABB* CastMouseRay(Cursor* cursor, Camera* camera, float screenWidth, float screenHeight);
  void Clear();

  // cast a ray, returns closest intersctable hit or NULL
  AABB* CastRay(glm::vec3 from, glm::vec3 ray);
private:
  std::vector<Intersectable> m_intersectables;
  glm::vec3 computeMouseRay(Cursor* cursor, Camera* camera, float screenWidth, float screenHeight);
};

