#pragma once
#include "command.h"

class MegaRig;
class MousePicker;

class FilePickerCommand : public Command
{
public:
  FilePickerCommand(MegaRig** rig, MousePicker* mp);
  ~FilePickerCommand();

  // Inherited via Command
  virtual void execute() override;
private:
  MegaRig** m_megaRig;
  MousePicker* m_mousePicker;
};

