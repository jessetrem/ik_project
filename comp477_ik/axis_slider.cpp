#include "axis_slider.h"
#include "mouse_picker.h"
#include "renderer.h"


AxisSlider::AxisSlider(glm::vec3 axis, MousePicker * mousePicker, glm::vec3 position, glm::vec3 bounds, glm::vec3 color) : m_handle(mousePicker->CreateHandle(position, bounds, color)), m_axis(axis)
{
  m_handle->RegisterDragable(this);
}

AxisSlider::~AxisSlider()
{
}

void AxisSlider::OnDrag(float deltaX, float deltaY)
{
  float deltaAvg = (deltaX + deltaY) / 250; // reduce average
  m_handle->SetPosition(m_handle->GetPosition() + m_axis * deltaAvg);
}

void AxisSlider::SetPosition(glm::vec3 position)
{
  m_handle->SetPosition(position);
}

glm::vec3 AxisSlider::GetPosition()
{
  return m_handle->GetPosition();
}

void AxisSlider::Draw(Renderer * renderer)
{
  m_handle->Draw(renderer);
}


