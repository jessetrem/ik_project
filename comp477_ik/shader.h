#pragma once
#include <string>
#include <glm/glm.hpp>


/**
 * @brief Object oriented abstraction of opengl shader program
 * 
 */
class Shader
{
public:
  Shader(std::string file);
  ~Shader();
  void setUniformFv(std::string name, glm::vec4 &vector);
  void setUniformFv(std::string name, glm::vec3 &vector);
  void setUniformFm(std::string name, glm::mat4x4 &matrix);
  void setUniformF(std::string name, float &number);
  void setUniform1i(std::string name, int texUnit);
  void bind();
  void unbind();
private:
  unsigned int load_shader(std::string shaderStr, unsigned int type);
  void build_program(unsigned int vert, unsigned int frag);
  void load_file(std::string file);
  unsigned int shader_program;
};