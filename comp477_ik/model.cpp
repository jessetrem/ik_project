#include "model.h"
#include "vertex_array.h"
#include "vertex_buffer.h"
#include "vertex_buffer_layout.h"
#include "index_buffer.h"


Model::Model() : m_data(NULL), m_indicies(NULL), m_vao(NULL), m_vbo(NULL)
{
}

Model::Model(BasicVertex* data, unsigned int dataSize, unsigned int* indicies, unsigned int indexSize) : Model()
{
  SetData(data, dataSize, indicies, indexSize);
}


Model::~Model()
{
  //delete[] m_data;
  //delete m_indicies;
  //delete m_vbo;
  //delete m_vao;
}

void Model::Bind()
{
  m_vao->bind();
  m_indicies->bind();
}

// TODO create Index Buffer in here and not as argument
void Model::SetData(BasicVertex* data, unsigned int dataSize, unsigned int* indicies, unsigned int indexSize)
{
  m_indexCount = indexSize;
  m_vertexCount = dataSize;
  m_data = data;
  m_indicies = new IndexBuffer();
  m_indicies->setData(indicies, indexSize * sizeof(unsigned int));
  BufferLayout bl(1);
  bl.pushFloat(3);
  m_vbo = new VertexBuffer();
  m_vbo->setData(data, dataSize * sizeof(BasicVertex));
  m_vao = new VertexArray();
  m_vao->setData(*m_vbo, bl, *m_indicies);
}

void Model::SetTransform(glm::mat4 transform)
{
  m_worldTransform = transform;
}

glm::mat4 Model::GetTransform()
{
  return glm::mat4();
}

unsigned int Model::GetIndexSize()
{
  return m_indexCount;
}
