#version 400 core

layout (location = 0) in vec3 vert;

uniform mat4 cam;
uniform mat4 world;

uniform float range;


void main() {
  vec4 newPos = cam * world * vec4(vert, 1.0);
  gl_Position = newPos;
}

~~pixel_boi~~
#version 400 core
out vec4 color;
uniform vec3 fill_color;

void main() {
  color = vec4(fill_color, 1.0f);
}