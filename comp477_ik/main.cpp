#include <iostream>
#include <GLEW/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include "input_handler.h"
#include "cursor.h"
#include "application.h"

// TODO port over what can be reused from CG renderer, make a repo publish to gitlab or github :)
int main(void)
{
	GLFWwindow* window;
  InputHandler ih;
  InputHandler::setActive(&ih);
  Cursor cur;
  Cursor::setCursor(&cur);
  

	if (!glfwInit())
		return -1;

	window = glfwCreateWindow(1280, 720, "COMP 477 IK project", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return -1;
	}
  /* Make the window's context current */
  glfwMakeContextCurrent(window);

  GLenum err = glewInit();
  if (GLEW_OK != err)
  {
    /* Problem: glewInit failed, something is seriously wrong. */
    fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
  }

  // REGISTER INPUT METHODS WITH GLFW
  // bind input handler to kb callback
  glfwSetKeyCallback(window, [](GLFWwindow* window, int key, int scancode, int action, int mods) {
    InputHandler *ih_carried = InputHandler::getActive();
    ih_carried->process(key, scancode, action, mods);
  });
  // bind input handler to mouse click callback
  glfwSetMouseButtonCallback(window, [](GLFWwindow* window, int key, int action, int mods) {
    InputHandler *ih_carried = InputHandler::getActive();
    ih_carried->process(key, -1, action, mods);
  });
  // bind cursor object to cursor movement
  glfwSetCursorPosCallback(window, [](GLFWwindow* window, double xpos, double ypos) {
    Cursor *c = Cursor::getCursor();
    c->update(xpos, ypos);
  });

  Application app(&ih, &cur);
  app.Init();
	/* Loop until the user closes the window */
	while (!glfwWindowShouldClose(window))
	{
		/* Render here */
		glClearColor(0.1, 0.1, 0.1, 0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    app.Update();
    app.Render();
		/* Swap front and back buffers */
		glfwSwapBuffers(window);

		/* Poll for and process events */
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}
