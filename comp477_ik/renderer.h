#include "./shader.h"
class VertexArray;
class IndexBuffer;
class Shader;
class Camera;
class Model;

class Renderer
{
public:
  ~Renderer();
  void init();
  void setCamera(Camera* cam);
  void drawObject(Model* model, Shader* shader);
private:
  Camera* m_cam;
};
