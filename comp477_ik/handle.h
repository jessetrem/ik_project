#pragma once
#include <vector>
#include <glm/glm.hpp>
#include "model.h"
class Renderer;
class Cursor;
class Shader;
class AABB;

enum Axis
{
  X, Y, Z
};

class Dragable
{
public:
  virtual void OnDrag(float deltaX, float deltaY) = 0;
};

class Selectable
{
public:
  virtual void OnMouseEnter() = 0;
  virtual void OnMouseLeave() = 0;
  virtual void OnSelect() = 0;
  virtual void OnRelease() = 0;
};

class Handle
{
public:
  Handle(Cursor* cursor, glm::vec3 position = { 0, 0, 0 }, glm::vec3 bounds = { 1, 1, 1 }, glm::vec3 color = { 0.8f, 0.8f, 0.8f });
  ~Handle();
  void Select();
  void Release();
  void HoverEnter();
  void HoverExit();
  void Update();
  bool Selected();
  void Draw(Renderer* renderer);
  glm::vec3 GetPosition();
  void SetPosition(glm::vec3 newPos);
  AABB* GetBoundingBox();

  void RegisterSelectable(Selectable* sel);
  void RegisterDragable(Dragable* drag);

  static void Init(); // initialize open gl resources
private:
  bool m_hovered;
  bool m_selected;
  AABB* m_boundingBox;

  std::vector<Selectable*> m_selectables;
  std::vector<Dragable*> m_dragables;

  Cursor* m_cursor;

  // void checkHover();
  // void updatePosition();
  void mouseEnter();
  void mouseLeave();

  // static rendering resources
  static Model* HANDLE_MODEL;
  static Shader* HANDLE_SHADER;
  static BasicVertex HANDLE_VERTS[];
  static unsigned int HANDLE_INDICIES[];


};
