#include <GLEW/glew.h>
#include "./vertex_array.h"
#include "./vertex_buffer.h"
#include "./vertex_buffer_layout.h"
#include "./index_buffer.h"

VertexArray::VertexArray() {
  glGenVertexArrays(1, &vao);
}

VertexArray::~VertexArray() {
  glDeleteVertexArrays(1, &vao);
}

void VertexArray::bind() {
  glBindVertexArray(vao);
}

void VertexArray::unbind() {
  glBindVertexArray(0);
}

// TODO remove index buffer since that is not bound to the vao
void VertexArray::setData(VertexBuffer &vb, BufferLayout &vbl, IndexBuffer &ib) {
  bind();
  vb.bind();
  setAttributes(vbl);
  vb.unbind();
  unbind();
}

void VertexArray::setAttributes(BufferLayout &vbl) {
  unsigned int offset = 0;
  for (unsigned int i = 0; i < vbl.getSize(); ++i) {
    glEnableVertexAttribArray(i);
    glVertexAttribPointer(i, vbl[i].count, vbl[i].type, GL_FALSE, vbl.getStride(), (void *)offset);
    offset += vbl[i].size * vbl[i].count;
  }
}