#include <glm/glm.hpp>
#include "sub_base.h"
#include "rig.h"


SubBase::SubBase()
{
}

SubBase::SubBase(std::vector<Rig*> rigs)
{
  for (std::vector<Rig*>::iterator i = rigs.begin(); i != rigs.end(); ++i)
  {
    AddRig(*i);
  }
}


SubBase::~SubBase()
{
}

void SubBase::AddRig(Rig * rig)
{
  m_rigs.push_back(rig);
}

void SubBase::ComputeUpdatedOrigin()
{
  glm::vec3 centroid = { 0, 0, 0 };
  for (std::vector<Rig*>::iterator i = m_rigs.begin(); i != m_rigs.end(); ++i)
  {
    centroid += (*i)->GetRootPosition();
  }
  centroid = { centroid.x / m_rigs.size(), centroid.y / m_rigs.size(), centroid.z / m_rigs.size() };
  for (std::vector<Rig*>::iterator i = m_rigs.begin(); i != m_rigs.end(); ++i)
  {
    (*i)->ComputeBackward(centroid);
  }
}
