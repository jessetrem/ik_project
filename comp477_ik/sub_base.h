#pragma once
#include <vector>
struct Joint;
class Rig;

class SubBase
{
public:
  SubBase();
  SubBase(std::vector<Rig*> rigs);
  ~SubBase();
  void AddRig(Rig* rig);
  void ComputeUpdatedOrigin();
private:
  std::vector<Rig*> m_rigs;
};

