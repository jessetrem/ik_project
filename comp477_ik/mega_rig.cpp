#include <fstream>
#include "bone.h"
#include "mega_rig.h"
#include "end_effector.h"
#include "rig.h"
#include "sub_base.h"

MegaRig::MegaRig(MousePicker * mp, std::string fileName) : m_root(new SubBase())
{
  Load(mp, fileName);
}

MegaRig::~MegaRig()
{
  for (int i = 0; i < m_rigs.size(); ++i)
  {
    delete m_rigs[i];
    delete m_endEffectors[i];
  }
}

void MegaRig::Load(MousePicker* mp, std::string filename)
{
  std::ifstream input("./rigs/" + filename);
  if (!input.good())
  {
    Load(mp, "default.rig");
    return;
  }
  Rig* currentRig = NULL;
  Joint* lastJoint = NULL;
  float x;
  float y;
  float z;
  input >> x >> y >> z;
  glm::vec3 rootPosition = { x, y, z };

  std::string type;
  int currentRigIndex = -1;
  int lineRigIndex;
  bool constrained;
  float c1, c2, c3, c4;
  while (input >> lineRigIndex >> x >> y >> z >> constrained >> c1 >> c2 >> c3 >> c4)
  {
    if (lineRigIndex != currentRigIndex)
    {
      if (currentRig)
      {
        m_rigs.push_back(currentRig);
        m_endEffectors.push_back(new EndEffector(lastJoint->position, mp));
        m_root->AddRig(currentRig);
      }
      currentRig = new Rig();
      currentRig->AddJoint(new Joint(rootPosition, false));
    }
    currentRigIndex = lineRigIndex;
    lastJoint = new Joint({ x, y, z }, constrained, c1, c2, c3, c4);
    currentRig->AddJoint(lastJoint);
  }
  m_rigs.push_back(currentRig);
  m_endEffectors.push_back(new EndEffector(lastJoint->position, mp));
  m_root->AddRig(currentRig);
}

void MegaRig::ComputeFABRIK()
{
  // don't compute centroid if only one rig exists
  if (m_rigs.size() > 1)
  {
    for (unsigned int i = 0; i < m_rigs.size(); ++i)
    {
      glm::vec3 target = m_endEffectors[i]->GetPosition();
      m_rigs[i]->ComputeForward(target);
    }
    m_root->ComputeUpdatedOrigin();
  }
  for (std::vector<Rig*>::iterator i = m_rigs.begin(); i != m_rigs.end(); ++i)
  {
    (*i)->ComputeFABRIK();
  }
}

void MegaRig::Update()
{
  for (unsigned int i = 0; i < m_rigs.size(); ++i)
  {
    m_endEffectors[i]->Update();
    m_rigs[i]->SetTarget(m_endEffectors[i]->GetPosition());
  }
}

void MegaRig::Draw(Renderer * renderer)
{
  for (std::vector<EndEffector*>::iterator i = m_endEffectors.begin(); i != m_endEffectors.end(); ++i)
  {
    (*i)->Draw(renderer);
  }
  for (std::vector<Rig*>::iterator i = m_rigs.begin(); i != m_rigs.end(); ++i)
  {
    (*i)->Draw(renderer);
  }
}
