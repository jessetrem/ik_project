#pragma once
#include <glm/glm.hpp>
#include "command.h"

class InputHandler;
class Cursor;

/**
 * @brief Class to represent a combination of a perspective and lookat matrix to act as a camera 
 */
class Camera
{
public:
  Camera(glm::vec3 startPos, float vert_angle, float hori_angle, float aspect, float fov, Cursor *cur);
  ~Camera();
  glm::mat4x4 getMVP();
  glm::mat4x4 getProjection();
  glm::mat4x4 getView();
  /**
   * @brief Function to give the camera a velocity that will be aplied when update is called in the direction the camera is facing
   * 
   * @param transformation the velocity vector
   */
  void move(glm::vec3 transformation);
  /**
   * @brief method to give the camera a tilt velocity
   * 
   * @param angle the tilt velocity in radians
   */
  void tilt(float angle);
  /**
   * @brief method to give the camera a rotation velocity
   * 
   * @param angle the rotation velocity in radians
   */
  void rotate(float angle);
  /**
   * @brief method to zero out all camera velocities
   * 
   */
  void stop();
  /**
   * @brief Set the Zoom Mode object
   * 
   * @param enable weather cursor zoom mode is enabled
   */
  void setZoomMode(bool enable);

  glm::vec3 & getPosition();
  
  /**
   * @brief method that applies all velocities to the camera
   * 
   */
  void update();
private:
  glm::vec3 position;
  glm::vec3 velocity;
  glm::vec3 direction;
  float vert_angle;
  float vert_velocity;
  float hori_angle;
  float hori_velocity;
  float aspect;
  float fov;
  bool zoom_mode;
  Cursor *cur;
};

/**
 * @brief command to move set the movement velocity of a camera
 * 
 */
class CameraMoveCommand : public Command 
{
public:
  CameraMoveCommand(glm::vec3 velocity, Camera *cam);
  ~CameraMoveCommand();
  void execute() override;
private:
  glm::vec3 velocity;
  Camera *cam;
};

/**
 * @brief command to set the rotation velocity of a camera
 * 
 */
class CameraRotateCommand : public Command 
{
public:
  CameraRotateCommand(float hori_velocity, float vert_velocity, Camera *cam);
  ~CameraRotateCommand();
  void execute() override;
private:
  float hori;
  float vert;
  Camera *cam;
};

/**
 * @brief command to enable zoom mode on a camera
 * 
 */
class CameraZoomCommand : public Command
{
  public:
  CameraZoomCommand(Camera *cam, bool enable);
  ~CameraZoomCommand();
  void execute() override;
private:
  Camera *cam;
  bool enable;
};
