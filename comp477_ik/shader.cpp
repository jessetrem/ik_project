#include <GLEW/glew.h>
#include <fstream>
#include <iostream>
#include <glm/gtc/type_ptr.hpp>
#include "./shader.h"

Shader::Shader(std::string file) {
  load_file(file);
}

Shader::~Shader() {
  glDeleteProgram(shader_program);
}

void Shader::bind() {
  glUseProgram(shader_program);
}

void Shader::unbind() {
  glUseProgram(0);  
}

void Shader::load_file(std::string file) {
  std::ifstream fileStream(file);
  std::string line = "";
  std::string vertex = "";
  std::string fragment = "";
  std::string *target = &vertex;
  std::cout << "loading shaders..." << std::endl;
  while(std::getline(fileStream, line)) {
    if (line == "~~pixel_boi~~") {
      target = &fragment;
    } else {
      (*target) += line + "\n";
    }
  }
  std::cout << "Vert:\n" << vertex << std::endl;
  unsigned int vertShader = load_shader(vertex, GL_VERTEX_SHADER);
  std::cout << "Frag:\n" << fragment << std::endl;
  unsigned int fragShader = load_shader(fragment, GL_FRAGMENT_SHADER);
  build_program(vertShader, fragShader);

  glDeleteShader(vertShader);
  glDeleteShader(fragShader);
}

unsigned int Shader::load_shader(std::string shaderStr, unsigned int type) {
  GLint success;
  GLchar infoLog[512];
  unsigned int shader = glCreateShader(type);
  char const * shaderSrc = shaderStr.c_str();

  glShaderSource(shader, 1, &shaderSrc, NULL);
  glCompileShader(shader);
  glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(shader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
	}

  return shader; 
}

void Shader::build_program(unsigned int vert, unsigned int frag) {
  GLint success;
  GLchar infoLog[512];
  
  shader_program = glCreateProgram();
  glAttachShader(shader_program, vert);
  glAttachShader(shader_program, frag);
  glLinkProgram(shader_program);

  glGetProgramiv(shader_program, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(shader_program, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
	}
}

void Shader::setUniformFv(std::string name, glm::vec4 &vector) {
  bind();
  int location = glGetUniformLocation(shader_program, name.c_str());
  glUniform4fv(location, 1, glm::value_ptr(vector));
  unbind();
}

void Shader::setUniformFv(std::string name, glm::vec3 &vector) {
  bind();
  int location = glGetUniformLocation(shader_program, name.c_str());
  glUniform3fv(location, 1, glm::value_ptr(vector));
  unbind();
}

void Shader::setUniformFm(std::string name, glm::mat4x4 &matrix) {
  bind();
  int location = glGetUniformLocation(shader_program, name.c_str());
  glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(matrix));
  unbind();
}

void Shader::setUniformF(std::string name, float &number) {
  bind();
  int location = glGetUniformLocation(shader_program, name.c_str());
  glUniform1f(location, number);
  unbind();
}

void Shader::setUniform1i(std::string name, int num) {
  bind();
  int location = glGetUniformLocation(shader_program, name.c_str());
  glUniform1i(location, num);
  unbind();
}
