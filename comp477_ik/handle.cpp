#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtc/matrix_transform.hpp>
#include "handle.h"
#include "shader.h"
#include "model.h"
#include "renderer.h"
#include "aabb.h"
#include "cursor.h"

Shader* Handle::HANDLE_SHADER = NULL;
Model* Handle::HANDLE_MODEL = NULL;

BasicVertex Handle::HANDLE_VERTS[8] = 
{
  { { -1, 1, 1 } },
  { { 1, -1, 1 } },
  { { -1, -1, 1 } },
  { { 1, 1, 1 } },
  { { -1, 1, -1 } },
  { { 1, 1, -1 } },
  { { -1, -1, -1 } },
  { { 1, -1, -1 } }
};

unsigned int Handle::HANDLE_INDICIES[36] =
{
  0, 1, 2,
  0, 3, 1,
  0, 4, 3,
  4, 5, 3,
  5, 1, 3,
  5, 7, 1,
  4, 2, 6,
  0, 2, 4,
  1, 6, 2,
  7, 6, 1,
  7, 4, 6,
  5, 4, 7
};

void Handle::Init()
{
  Handle::HANDLE_MODEL = new Model(HANDLE_VERTS, 8, HANDLE_INDICIES, 36);
  Handle::HANDLE_SHADER = new Shader("./shaders/basic.shader");
}

Handle::Handle(Cursor * cursor, glm::vec3 position, glm::vec3 bounds, glm::vec3 color) : m_cursor(cursor), m_boundingBox(new AABB(position, bounds.x, bounds.y, bounds.z, color))
{
}

Handle::~Handle()
{
}

void Handle::Select()
{
  m_selected = true;
  for (std::vector<Selectable*>::iterator itt = m_selectables.begin(); itt != m_selectables.end(); ++itt)
  {
    (*itt)->OnSelect();
  }
}

void Handle::Release()
{
  m_selected = false;
  for (std::vector<Selectable*>::iterator itt = m_selectables.begin(); itt != m_selectables.end(); ++itt)
  {
    (*itt)->OnRelease();
  }
}

void Handle::HoverEnter()
{
  m_boundingBox->color = m_boundingBox->color * 2.0f;
}

void Handle::HoverExit()
{
  m_boundingBox->color = m_boundingBox->color * 0.5f;
}

void Handle::Update()
{
  if (m_selected)
  {
    for (std::vector<Dragable*>::iterator dragged = m_dragables.begin(); dragged != m_dragables.end(); ++dragged)
    {
      (*dragged)->OnDrag(m_cursor->getDeltaX(), m_cursor->getDeltaY());
    }
  }
}

bool Handle::Selected()
{
  return m_selected;
}

void Handle::Draw(Renderer * renderer)
{
  m_boundingBox->Draw(renderer);
}

glm::vec3 Handle::GetPosition()
{
  return m_boundingBox->position;
}

void Handle::SetPosition(glm::vec3 newPos)
{
  m_boundingBox->position = newPos;
}

AABB * Handle::GetBoundingBox()
{
  return m_boundingBox;
}

void Handle::RegisterSelectable(Selectable * sel)
{
  m_selectables.push_back(sel);
}

void Handle::RegisterDragable(Dragable * drag)
{
  m_dragables.push_back(drag);
}

void Handle::mouseEnter()
{
  m_hovered = true;
  for (std::vector<Selectable*>::iterator itt = m_selectables.begin(); itt != m_selectables.end(); ++itt)
  {
    (*itt)->OnMouseEnter();
  }
}

void Handle::mouseLeave()
{
  m_hovered = false;
  for (std::vector<Selectable*>::iterator itt = m_selectables.begin(); itt != m_selectables.end(); ++itt)
  {
    (*itt)->OnMouseLeave();
  }
}
