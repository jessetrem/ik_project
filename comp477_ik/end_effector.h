#pragma once
#include <glm/glm.hpp>
#include "axis_slider.h"
class MousePicker;
class Renderer;

class EndEffector
{
public:
  EndEffector(glm::vec3 position, MousePicker* mousePicker);
  ~EndEffector();
  void Update();
  void Draw(Renderer* renderer);
  glm::vec3 GetPosition();
private:
  AxisSlider m_xSlider;
  AxisSlider m_ySlider;
  AxisSlider m_zSlider;
  glm::vec3 m_position;
};

