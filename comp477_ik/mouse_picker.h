#pragma once
#include <unordered_map>
#include <glm/glm.hpp>
#include "command.h"
class RayCaster;
class Handle;
class Cursor;
class Camera;
struct AABB;

class MousePicker
{
public:
  MousePicker(Cursor* cursor, Camera* camera);
  ~MousePicker();
  // handle factory
  Handle* CreateHandle(glm::vec3 position, glm::vec3 bounds, glm::vec3 color);
  void Clear();
  void RegisterHandle(Handle* handle);
  void MouseDown();
  void MouseUp();
  void Update();
private:
  std::unordered_map<AABB*, Handle*> m_handleMap;
  Handle* m_currentSelection;
  RayCaster* m_rayCaster;
  Cursor* m_cursor;
  Camera* m_camera;
};

class MousePickerClickCommand : public Command
{
public:
  MousePickerClickCommand(MousePicker* mp);
  // Inherited via Command
  virtual void execute() override;
private:
  MousePicker* m_mousePicker;
};

class MousePickerReleaseCommand : public Command
{
public:
  MousePickerReleaseCommand(MousePicker* mp);
  // Inherited via Command
  virtual void execute() override;
private:
  MousePicker* m_mousePicker;
};
