#include "end_effector.h"



EndEffector::EndEffector(glm::vec3 position, MousePicker * mousePicker) : m_xSlider({ 1, 0, 0 }, mousePicker, { position.x + 1, position.y, position.z }, { 2, 0.25, 0.25 }, {0.5, 0, 0}),
m_ySlider({ 0, -1, 0 }, mousePicker, { position.x, position.y + 1, position.z }, {  0.25, 2, 0.25 }, { 0, 0.5, 0 }),
m_zSlider({ 0, 0, 1 }, mousePicker, { position.x, position.y, position.z + 1 }, { 0.25, 0.25, 2 }, { 0, 0, 0.5 }),
m_position(position)
{
}

EndEffector::~EndEffector()
{
}

// this whole method is sort of hacky if anyone has a better idea please fix it :)
void EndEffector::Update()
{
  // get updated position
  m_position = { m_xSlider.GetPosition().x - 1, m_ySlider.GetPosition().y - 1, m_zSlider.GetPosition().z - 1 };
  // update old positions
  m_xSlider.SetPosition({ m_position.x + 1, m_position.y, m_position.z });
  m_ySlider.SetPosition({ m_position.x, m_position.y + 1, m_position.z });
  m_zSlider.SetPosition({ m_position.x, m_position.y, m_position.z + 1 });
}

void EndEffector::Draw(Renderer * renderer)
{
  m_xSlider.Draw(renderer);
  m_ySlider.Draw(renderer);
  m_zSlider.Draw(renderer);
}

glm::vec3 EndEffector::GetPosition()
{
  return m_position;
}
