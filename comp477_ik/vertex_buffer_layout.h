#pragma once
struct AttributePointer
{
  unsigned int size;
  unsigned int count;
  unsigned int type;
  bool normalized;
};

/**
 * @brief Object oriented abstraction of vertex buffer pointer definitions
 * 
 */
class BufferLayout
{
public:
  BufferLayout(unsigned int size = 1);
  ~BufferLayout();
  void pushFloat(unsigned int count);
  AttributePointer & operator[](unsigned int index);
  unsigned int getStride();
  unsigned int getSize();
private:
  unsigned int size;
  unsigned int stride;
  unsigned int buffer_layout_size;
  unsigned int push_pos;
  AttributePointer * elements;
};