#include <GLEW/glew.h>
#include "./index_buffer.h"


IndexBuffer::IndexBuffer()
{
  glGenBuffers(1, &ib);
}

IndexBuffer::~IndexBuffer()
{
  glDeleteBuffers(1, &ib);
}

unsigned int IndexBuffer::getSize() {
  return m_size;
}

void IndexBuffer::bind()
{
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib);
}

void IndexBuffer::unbind()
{
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void IndexBuffer::setData(unsigned int *data, unsigned int size)
{
  bind();
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
  m_size = size;
  unbind();
}
