#pragma once
#include "handle.h"
#include <glm/glm.hpp>
class Handle;
class MousePicker;
class Renderer;

class AxisSlider : public Dragable
{
public:
  AxisSlider(glm::vec3 axis, MousePicker* mousePicker, glm::vec3 position, glm::vec3 bounds, glm::vec3 color = {0.5, 0.5, 0.5});
  ~AxisSlider();

  // Inherited via Dragable
  virtual void OnDrag(float deltaX, float deltaY) override;

  // TODO find less hacky solution to updating sliders
  void SetPosition(glm::vec3 position);
  glm::vec3 GetPosition();

  void Draw(Renderer* renderer);

private:
  glm::vec3 m_axis;
  Handle* m_handle;
};

