#pragma once
#include <vector>
#include <string>
class EndEffector;
class Rig;
class SubBase;
class Renderer;
class MousePicker;

class MegaRig
{
public:
  MegaRig(MousePicker* mp, std::string fileName = "default.rig");
  ~MegaRig();
  void ComputeFABRIK();
  void Update();
  void Draw(Renderer* renderer);
private:
  void Load(MousePicker* mp, std::string filename);
  std::vector<EndEffector*> m_endEffectors;
  std::vector<Rig*> m_rigs;
  SubBase* m_root;
};

