#include <GLFW/glfw3.h>
#include <cmath>
#include "application.h"
#include "input_handler.h"
#include "cursor.h"
#include "renderer.h"
#include "camera.h"
#include "bone.h"
#include "rig.h"
#include "handle.h"
#include "aabb.h"
#include "ray_caster.h"
#include "mouse_picker.h"
#include "end_effector.h"
#include "mega_rig.h"
#include "file_picker_command.h"

Application::Application(InputHandler * controller, Cursor* cursor) : m_controller(controller), m_cursor(cursor)
{
}

Application::~Application()
{
  delete m_rig;
  delete c_camBackward;
  delete c_camForward;
  delete c_camLeft;
  delete c_camRight;
  delete c_camRotateLeft;
  delete c_camRotateRight;

  // release bone GPU resources
  Bone::Destroy();
}

void Application::Init()
{
  m_renderer = new Renderer();
  m_camera = new Camera({ 0, 0, 5 }, 0, 0, 16.0f / 9.0f, 0.883572934, NULL);
  m_renderer->init();
  m_renderer->setCamera(m_camera);

  m_mousePicker = new MousePicker(m_cursor, m_camera);

  // declare commands
  c_camRight = new CameraMoveCommand({ 0.05f, 0, 0 }, m_camera);
  c_camLeft = new CameraMoveCommand({ -0.05f, 0, 0 }, m_camera);
  c_camForward = new CameraMoveCommand({ 0, 0, -0.05f }, m_camera);
  c_camBackward = new CameraMoveCommand({ 0, 0, 0.05f }, m_camera);
  c_camRotateLeft = new CameraRotateCommand(0.01f, 0.0f, m_camera);
  c_camRotateRight = new CameraRotateCommand(-0.01f, 0.0f, m_camera);
  c_camRotateUp = new CameraRotateCommand(0.0f, 0.01f, m_camera);
  c_camRotateDown = new CameraRotateCommand(0.0f, -0.01f, m_camera);
  c_mouseClickPick = new MousePickerClickCommand(m_mousePicker);
  c_mouseClickRelease = new MousePickerReleaseCommand(m_mousePicker);
  c_filePicker = new FilePickerCommand(&m_megaRig, m_mousePicker);
  // register commands
  m_controller->registerKey(GLFW_KEY_A, GLFW_PRESS, c_camLeft);
  m_controller->registerKey(GLFW_KEY_D, GLFW_PRESS, c_camRight);
  m_controller->registerKey(GLFW_KEY_W, GLFW_PRESS, c_camForward);
  m_controller->registerKey(GLFW_KEY_S, GLFW_PRESS, c_camBackward);
  // register inverse on release
  m_controller->registerKey(GLFW_KEY_A, GLFW_RELEASE, c_camRight);
  m_controller->registerKey(GLFW_KEY_D, GLFW_RELEASE, c_camLeft);
  m_controller->registerKey(GLFW_KEY_W, GLFW_RELEASE, c_camBackward);
  m_controller->registerKey(GLFW_KEY_S, GLFW_RELEASE, c_camForward);
  // register rotations
  m_controller->registerKey(GLFW_KEY_LEFT, GLFW_PRESS, c_camRotateLeft);
  m_controller->registerKey(GLFW_KEY_LEFT, GLFW_RELEASE, c_camRotateRight);
  m_controller->registerKey(GLFW_KEY_RIGHT, GLFW_PRESS, c_camRotateRight);
  m_controller->registerKey(GLFW_KEY_RIGHT, GLFW_RELEASE, c_camRotateLeft);
  // register inverse on release
  m_controller->registerKey(GLFW_KEY_UP, GLFW_PRESS, c_camRotateUp);
  m_controller->registerKey(GLFW_KEY_UP, GLFW_RELEASE, c_camRotateDown);
  m_controller->registerKey(GLFW_KEY_DOWN, GLFW_PRESS, c_camRotateDown);
  m_controller->registerKey(GLFW_KEY_DOWN, GLFW_RELEASE, c_camRotateUp);
  // register mouse picker commands
  m_controller->registerKey(GLFW_MOUSE_BUTTON_1, GLFW_PRESS, c_mouseClickPick);
  m_controller->registerKey(GLFW_MOUSE_BUTTON_1, GLFW_RELEASE, c_mouseClickRelease);
  // register file picker command
  m_controller->registerKey(GLFW_KEY_L, GLFW_PRESS, c_filePicker);

  // Initialize static opengl resources
  Bone::Init();
  Handle::Init();
  AABB::Init();

  //m_rig = new Rig();
  //m_endEffector = new EndEffector({ 0, 3, 3 }, m_mousePicker);
  m_megaRig = new MegaRig(m_mousePicker);
}

void Application::Update()
{
  m_camera->update();
  m_mousePicker->Update();
  m_megaRig->Update();
  m_megaRig->ComputeFABRIK();
  // m_rig->SetTarget(m_endEffector->GetPosition());
  // m_rig->ComputeFABRIK();
  

  //m_endEffector->Update();

  m_cursor->FlushDelta();
}

void Application::Render()
{
  m_megaRig->Draw(m_renderer);
  // m_rig->Draw(m_renderer);
  //m_endEffector->Draw(m_renderer);
}
