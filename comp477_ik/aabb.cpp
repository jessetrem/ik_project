#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtc/matrix_transform.hpp>
#include "aabb.h"
#include "shader.h"
#include "renderer.h"

Shader* AABB::SHADER = NULL;
Model* AABB::MODEL = NULL;

BasicVertex AABB::VERTS[8] =
{
  { { -0.5, 0.5, 0.5 } },
  { { 0.5, -0.5, 0.5 } },
  { { -0.5, -0.5, 0.5 } },
  { { 0.5, 0.5, 0.5 } },
  { { -0.5, 0.5, -0.5 } },
  { { 0.5, 0.5, -0.5 } },
  { { -0.5, -0.5, -0.5 } },
  { { 0.5, -0.5, -0.5 } }
};

unsigned int AABB::INDICIES[36] =
{
  0, 1, 2,
  0, 3, 1,
  0, 4, 3,
  4, 5, 3,
  5, 1, 3,
  5, 7, 1,
  4, 2, 6,
  0, 2, 4,
  1, 6, 2,
  7, 6, 1,
  7, 4, 6,
  5, 4, 7
};

void AABB::Init()
{
  AABB::MODEL = new Model(VERTS, 8, INDICIES, 36);
  AABB::SHADER = new Shader("./shaders/basic.shader");
}



AABB::AABB() : AABB({0, 0, 0}, 1, 1, 1)
{
}

AABB::AABB(glm::vec3 position, float width, float height, float thickness, glm::vec3 color) : position(position), width(width), height(height), thickness(thickness), color(color)
{
}


AABB::~AABB()
{
}

glm::vec3 AABB::GetMin()
{
  return { position.x - width / 2, position.y - height / 2, position.z + thickness / 2 };
}

glm::vec3 AABB::GetMax()
{
  return { position.x + width / 2, position.y + height / 2, position.z - thickness / 2 };
}

void AABB::Draw(Renderer* renderer)
{
  glm::mat4 matScale = glm::scale(glm::mat4(1), { width, height, thickness});
  glm::mat4 matPosition = glm::translate(glm::mat4(1), position);
  glm::mat4 world = matPosition * matScale;
  SHADER->setUniformFm("world", world);
  SHADER->setUniformFv("fill_color", color);
  renderer->drawObject(MODEL, SHADER);
}
