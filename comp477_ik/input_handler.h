#include <unordered_map>
#pragma once
#include <string>

class Command;
/**
 * @brief struct for simple linked list of commands
 * 
 */
struct CommandChain {
  Command *current;
  CommandChain *next;
};
/**
 * @brief class that simplifies keyboard events from glfw allowing commands
 * to be stored in a map that is indexed by the glfw key enum
 * 
 */
// TODO: add ability to remove commands from input handler and safety when deleting commands
class InputHandler
{
public:
  InputHandler();
  ~InputHandler();
  void process(int key, int sc, int action, int mods);
  CommandChain * registerKey(int keyCode, int action, Command *command);
  static void setActive(InputHandler *ih);
  static InputHandler * getActive();
private:
  std::unordered_map<unsigned int, CommandChain*> keyboard[3];
  static InputHandler *active_input_handler;
};