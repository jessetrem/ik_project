#pragma once
#include <vector>
#include <glm/glm.hpp>
class Renderer;
class Bone;
struct Joint;

class Rig
{
public:
  // TODO load from some sort of file
  Rig();
  ~Rig();
  void Load();
  void Load2();
  void SetTarget(glm::vec3 position);
  void SetTarget(float x, float y, float z);
  void AddJoint(Joint* joint);
  void ComputeFABRIK();
  void ComputeForward(glm::vec3& target);
  void ComputeBackward(glm::vec3& target);
  glm::vec3 GetRootPosition();
  void Draw(Renderer* renderer);
private:
  std::vector<Bone*> m_bones;
  std::vector<Joint*> m_joints;
  glm::vec3 m_target;
  float m_error;
};

