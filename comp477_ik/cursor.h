class Cursor {
  /**
   * @brief Object that stores the current cursor position
   * 
   */
public:
  Cursor();
  ~Cursor();
  float getDeltaX();
  float getDeltaY();
  float getX();
  float getY();
  static Cursor * getCursor();
  static void setCursor(Cursor *c);
  void update(float x, float y);
  void FlushDelta();
private:
  float x;
  float y;
  float delta_x;
  float delta_y;
  static Cursor *cur;
};