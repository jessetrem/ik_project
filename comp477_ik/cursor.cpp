#include "./cursor.h"


Cursor::Cursor(): x(0), y(0), delta_x(0), delta_y(0) {}
Cursor::~Cursor() {}

Cursor* Cursor::getCursor() {
  return cur;
}

void Cursor::setCursor(Cursor *c) {
  cur = c;
}

void Cursor::update(float newX, float newY) {
  delta_x = newX - x;
  delta_y = newY - y;
  x = newX;
  y = newY;
}

void Cursor::FlushDelta()
{
  delta_x = 0;
  delta_y = 0;
}

float Cursor::getDeltaX() {
  int currDelta = delta_x;
  return currDelta;
}

float Cursor::getDeltaY() {
  int currDelta = delta_y;
  return currDelta;
}

float Cursor::getX()
{
  return x;
}

float Cursor::getY()
{
  return y;
}

Cursor * Cursor::cur = nullptr;
