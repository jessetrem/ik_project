#pragma once
#include <glm/glm.hpp>
class VertexArray;
class VertexBuffer;
class IndexBuffer;

struct BasicVertex
{
  glm::vec3 position;
};

class Model
{
public:
  Model();
  Model(BasicVertex* data, unsigned int dataSize, unsigned int* indicies, unsigned int indexSize);
  ~Model();
  void Bind();
  void SetData(BasicVertex * data, unsigned int dataSize, unsigned int* indicies, unsigned int indexSize);
  void SetTransform(glm::mat4 transform);
  glm::mat4 GetTransform();
  unsigned int GetIndexSize();
private:
  glm::mat4 m_worldTransform;
  unsigned int m_vertexCount;
  unsigned int m_indexCount;
  BasicVertex* m_data;
  VertexArray* m_vao;
  VertexBuffer* m_vbo;
  IndexBuffer* m_indicies;
};

