#pragma once
#include<glm/glm.hpp>
class InputHandler;
class Cursor;
class Renderer;
class Camera;
class CameraMoveCommand;
class CameraRotateCommand;
class Rig;
class Handle;
class AABB;
class RayCaster;
class MousePicker;
class MousePickerClickCommand;
class MousePickerReleaseCommand;
class EndEffector;
class MegaRig;
class FilePickerCommand;

class Application
{
public:
  Application(InputHandler* controller, Cursor* cursor);
  ~Application();
  void Init();
  void Update();
  void Render();
private:
  InputHandler* m_controller;
  Cursor* m_cursor;
  Camera* m_camera;
  Renderer* m_renderer;
  Rig* m_rig;
  MegaRig* m_megaRig;


  // camera commands
  CameraMoveCommand* c_camLeft;
  CameraMoveCommand* c_camRight;
  CameraMoveCommand* c_camForward;
  CameraMoveCommand* c_camBackward;
  // rotation commands
  CameraRotateCommand* c_camRotateLeft;
  CameraRotateCommand* c_camRotateRight;
  CameraRotateCommand* c_camRotateUp;
  CameraRotateCommand* c_camRotateDown;
  // mouse picker commands
  MousePickerClickCommand* c_mouseClickPick;
  MousePickerReleaseCommand* c_mouseClickRelease;
  FilePickerCommand* c_filePicker;



  //handles
  MousePicker* m_mousePicker;
  EndEffector* m_endEffector;

};

