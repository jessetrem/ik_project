#include <GLEW/glew.h>
#include <GLFW/glfw3.h>
#include "./renderer.h"
#include "./vertex_array.h"
#include "./index_buffer.h"
#include "./shader.h"
#include "camera.h"
#include "model.h"

glm::vec3 lightPos(0.0f, 25.0f, 0.0f);

Renderer::~Renderer() {
}

void Renderer::init() {
  glEnable(GL_DEPTH_TEST);
  // no face culling for this
  glEnable(GL_CULL_FACE);
  glFrontFace(GL_CW);
  glCullFace(GL_BACK);
  //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  glPointSize(10.0f);
  glLineWidth(10.0f);
}

void Renderer::setCamera(Camera* cam)
{
  m_cam = cam;
}

void Renderer::drawObject(Model* model, Shader* shader)
{
	// renderer goes here :)
  model->Bind();
  glm::mat4x4 cameraMVP = m_cam->getMVP();
  glm::mat4 id(1);
  shader->setUniformFm("cam", cameraMVP);
  shader->bind();

  glDrawElements(GL_TRIANGLES, model->GetIndexSize(), GL_UNSIGNED_INT, (void*)0); // (void*)0 ?? null
}
