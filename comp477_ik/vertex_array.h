#pragma once
class VertexBuffer;
class BufferLayout;
class IndexBuffer;

/**
 * @brief Object oriented abstraction of opengl VAO
 * 
 */
class VertexArray {
public:
  VertexArray();
  ~VertexArray();
  void bind();
  void unbind();
  void setData(VertexBuffer &vb, BufferLayout &vbl, IndexBuffer &ib);
private:
  void setAttributes(BufferLayout &vbl);
  unsigned int vao;
};