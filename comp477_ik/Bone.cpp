#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/projection.hpp>
#include <cmath>
#include "bone.h"
#include "renderer.h"
#include "shader.h"

Model* Bone::MODEL = NULL;
Shader* Bone::SHADER = NULL;

Bone::Bone() : Bone(1, { 0, 1, 0 }, { 0, 0, 0 })
{
}

Bone::Bone(float length, glm::vec3 direction, glm::vec3 position) : m_length(length), m_direction(glm::normalize(direction)), m_position(position)
{
}

Bone::Bone(Joint* start, Joint* end) : m_start(start), m_end(end), m_length(glm::length(end->position - start->position))
{
  ComputeModelTransforms();
}


Bone::~Bone()
{
}

void Bone::Init()
{
  Bone::MODEL = new Model(BONE_VERTICIES, 6, BONE_INDICIES, 24);
  Bone::SHADER = new Shader("./shaders/basic.shader");
}

void Bone::Destroy()
{
  delete Bone::MODEL;
  delete Bone::SHADER;
}

void Bone::AdjustForward()
{
  glm::vec3 targetEnd = m_end->position;
  glm::vec3 directionReaching = targetEnd - m_start->position;
  glm::vec3 directionLength = glm::normalize(directionReaching) * m_length;
  
  m_start->position = directionReaching - directionLength + m_start->position;
}

void Bone::AdjustConstraintsForward()
{
  
}

void Bone::AdjustBackward()
{
  glm::vec3 targetEnd = m_start->position;
  glm::vec3 directionReaching = targetEnd - m_end->position;
  glm::vec3 directionLength = glm::normalize(directionReaching) * m_length;

  m_end->position = directionReaching - directionLength + m_end->position;
  if (m_start->constrained)
  {
    AdjustConstraintsBackward();
  }
  ComputeModelTransforms();
  m_end->constraintDirection = m_direction;
}

void Bone::AdjustConstraintsBackward()
{
  if (glm::length(m_start->constraintDirection) == 0)
  {
    return;
  }
  glm::vec3 calc = m_end->position - m_start->position;

  // get coordDirections
  glm::vec3 rightVec = glm::cross({ 0, 1, 0 }, m_start->constraintDirection);
  glm::vec3 upVec = glm::cross(rightVec, m_start->constraintDirection);
  glm::vec3 leftVec = -rightVec;
  glm::vec3 downVec = -upVec;

  upVec = glm::length(upVec - calc) < glm::length(downVec - calc) ? upVec : downVec;
  rightVec = glm::length(rightVec - calc) < glm::length(leftVec - calc) ? rightVec : leftVec;

  float scalar = glm::dot(calc, m_start->constraintDirection);
  glm::vec3 core = m_start->constraintDirection * scalar;
  glm::vec3 adjust = calc - core;

  if (scalar < 0)
  {
    core = -core;
  }

  float xAspect = glm::dot(adjust, rightVec);
  float yAspect = glm::dot(adjust, upVec);

  float height = glm::length(core);

  float lengthUp = height * tanf(m_end->up);
  float lengthDown = -(height * tanf(m_end->down));
  float lengthRight = height * tanf(m_end->left);
  float lengthLeft = -(height * tanf(m_end->right));

  float xBound = xAspect >= 0 ? lengthRight : lengthLeft;
  float yBound = yAspect >= 0 ? lengthUp : lengthDown;

  float elipse = (xAspect * xAspect) / (xBound * xBound) + (yAspect * yAspect) / (yBound * yBound);
  if (elipse > 1 || scalar < 0)
  {
    float angle = atan2f(yAspect, xAspect);
    float xOffset = xBound * cosf(angle);
    float yOffset = yBound * sinf(angle);
    glm::vec3 offset = glm::normalize(core + (rightVec * xOffset) + (upVec * yOffset)) * m_length;

    m_end->position = m_start->position + offset;
  }
}

void Bone::ComputeModelTransforms()
{
  m_position = m_start->position;
  m_direction = glm::normalize(m_end->position - m_start->position);
}

// TODO bake shader into bone class
void Bone::Draw(Renderer* renderer)
{
  glm::mat4 rotation = f_getDirectionalRotation();
  glm::mat4 translation = glm::translate(glm::mat4(1), m_position);
  glm::mat4 scale = glm::scale(glm::mat4(1), { 1, m_length, 1 });
  glm::mat4 world = translation * rotation * scale;
  SHADER->setUniformFm("world", world);
  SHADER->setUniformFv("fill_color", BONE_COLOR);
  renderer->drawObject(MODEL, SHADER);
}

glm::mat4 Bone::f_getDirectionalRotation()
{
  glm::vec3 up = { 0, 1, 0 }; // up direction to rotate from
  glm::vec3 axis = glm::cross(up, m_direction); // axis is the normal of up and the direction you are facing
  float angle = acos(glm::dot(up, m_direction));
  // define world transforms
  glm::mat4 rotation = angle ? glm::rotate(glm::mat4(1), angle, axis) : glm::mat4(1);

  return rotation;
}

BasicVertex Bone::BONE_VERTICIES[6] = {
  { {0.0f, 0.0f, 0.0f } },
  { {0.2f, 0.2f, 0.2f } },
  { {0.2f, 0.2f, -0.2f } },
  { {-0.2f, 0.2f, -0.2f } },
  { {-0.2f, 0.2f, 0.2f } },
  { {0.0f, 1.0f, 0.0f } },
};

unsigned int Bone::BONE_INDICIES[24] = {
    0, 1, 2,
    0, 2, 3,
    0, 3, 4,
    0, 4, 1,
    5, 2, 1,
    5, 3, 2,
    5, 4, 3,
    5, 1, 4,
};

glm::vec3 Bone::BONE_COLOR = { 1, 1, 1 };

Joint::Joint() : Joint({0, 0, 0})
{
}

Joint::Joint(glm::vec3 position, bool constrained, float upRestriction, float downRestriction, float leftRestriction, float rightRestriction) : position(position), up(upRestriction), down(downRestriction), right(rightRestriction), left(leftRestriction), constraintDirection(0, 0, 0), constrained(constrained)
{
}

Joint::~Joint()
{
}
