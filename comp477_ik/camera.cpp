#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include "./camera.h"
#include "./cursor.h"

Camera::Camera(glm::vec3 startPos, float vert_angle, float hori_angle, float aspect, float fov, Cursor *cur): position(startPos), vert_angle(vert_angle), hori_angle(hori_angle), aspect(aspect), fov(fov), hori_velocity(0), vert_velocity(0), zoom_mode(false), cur(cur), velocity({ 0.0f, 0.0f, 0.0f })  {}
Camera::~Camera() {}

glm::mat4x4 Camera::getMVP() {
  glm::vec3 direction = glm::rotate(glm::vec3(0.0, 0.0, -1.0), vert_angle, glm::vec3(1.0f, 0.0f, 0.0f));
  direction = glm::rotate(direction, hori_angle, glm::vec3(0.0f, 1.0f, 0.0f));
  return glm::perspective(fov, aspect, 0.1f, 1000.0f) * glm::lookAt(position, direction + position, glm::vec3(0,1,0));
}

glm::mat4x4 Camera::getProjection()
{
  return glm::perspective(fov, aspect, 0.1f, 1000.0f);
}

glm::mat4x4 Camera::getView()
{
  glm::vec3 direction = glm::rotate(glm::vec3(0.0, 0.0, -1.0), vert_angle, glm::vec3(1.0f, 0.0f, 0.0f));
  direction = glm::rotate(direction, hori_angle, glm::vec3(0.0f, 1.0f, 0.0f));
  return glm::lookAt(position, direction + position, glm::vec3(0, 1, 0));
}

glm::vec3 & Camera::getPosition() {
  return position;
}

void Camera::move(glm::vec3 transformation) {
  velocity = velocity + transformation;
}

void Camera::tilt(float angle) {
  vert_velocity += angle;
}

void Camera::rotate(float angle) {
  hori_velocity += angle;
}

void Camera::setZoomMode(bool enable) {
  zoom_mode = enable;
}

void Camera::stop() {
  velocity = glm::vec3(0, 0, 0);
  vert_velocity = 0;
  hori_velocity = 0;
}

void Camera::update() {
  glm::vec3 myVelocity = velocity;
  if (cur && zoom_mode) {
    myVelocity += glm::vec3(0, 0, cur->getDeltaY() / 3);
  }
  glm::vec3 rotatedVelocity = glm::rotate(myVelocity, vert_angle, glm::vec3(1.0f, 0.0f, 0.0f));
  rotatedVelocity = glm::rotate(rotatedVelocity, hori_angle, glm::vec3(0.0f, 1.0f, 0.0f));
  position += rotatedVelocity;
  hori_angle += hori_velocity;
  vert_angle += vert_velocity;
}

CameraMoveCommand::CameraMoveCommand(glm::vec3 velocity, Camera *cam) : velocity(velocity), cam(cam) {}
CameraMoveCommand::~CameraMoveCommand() {}

void CameraMoveCommand::execute() {
  cam->move(velocity);
}

CameraRotateCommand::CameraRotateCommand(float hori_velocity, float vert_velocity, Camera *cam) : hori(hori_velocity), vert(vert_velocity), cam(cam) {}
CameraRotateCommand::~CameraRotateCommand() {}

void CameraRotateCommand::execute() {
  cam->rotate(hori);
  cam->tilt(vert);
}

CameraZoomCommand::CameraZoomCommand(Camera *cam, bool enable): cam(cam), enable(enable) {}
CameraZoomCommand::~CameraZoomCommand() {};

void CameraZoomCommand::execute() {
  cam->setZoomMode(enable);
}
