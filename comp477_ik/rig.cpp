#include "renderer.h"
#include "rig.h"
#include "bone.h"



Rig::Rig() : m_error(0.001)
{
}


Rig::~Rig()
{
  for (int i = 0; i < m_bones.size(); ++i)
  {
    delete m_bones[i];
  }
  for (int i = 0; i < m_joints.size(); ++i)
  {
    delete m_joints[i];
  }
}

void Rig::Load()
{
  // create joints
  m_joints.push_back(new Joint({ 0, 0, 0 }));
  m_joints.push_back(new Joint({ 0, 2, 0 }));
  m_joints.push_back(new Joint({ 0, 4, 0 }));
  m_joints.push_back(new Joint({ 0, 6, 0 }));
  m_joints.push_back(new Joint({ 0, 8, 0 }));
  m_joints.push_back(new Joint({ 0, 10, 0 }));
  m_joints.push_back(new Joint({ 0, 12, 0 }));

  // create bones
  m_bones.push_back(new Bone(m_joints[0], m_joints[1]));
  m_bones.push_back(new Bone(m_joints[1], m_joints[2]));
  m_bones.push_back(new Bone(m_joints[2], m_joints[3]));
  m_bones.push_back(new Bone(m_joints[3], m_joints[4]));
  m_bones.push_back(new Bone(m_joints[4], m_joints[5]));
  m_bones.push_back(new Bone(m_joints[5], m_joints[6]));
}

void Rig::Load2()
{
  // create joints
  m_joints.push_back(new Joint({ 0, 0, 0 }));
  m_joints.push_back(new Joint({ 2, 0, 0 }));
  m_joints.push_back(new Joint({ 4, 0, 0 }));
  m_joints.push_back(new Joint({ 6, 0, 0 }));
  m_joints.push_back(new Joint({ 8, 0, 0 }));

  // create bones
  m_bones.push_back(new Bone(m_joints[0], m_joints[1]));
  m_bones.push_back(new Bone(m_joints[1], m_joints[2]));
  m_bones.push_back(new Bone(m_joints[2], m_joints[3]));
  m_bones.push_back(new Bone(m_joints[3], m_joints[4]));
}

void Rig::SetTarget(glm::vec3 position)
{
  m_target = position;
}

void Rig::SetTarget(float x, float y, float z)
{
  SetTarget({ x, y, z });
}

void Rig::AddJoint(Joint * joint)
{
  float currentLength = m_joints.size();
  m_joints.push_back(joint);
  if (currentLength > 0)
  {
    m_bones.push_back(new Bone(m_joints[currentLength - 1], joint));
  }
}

void Rig::ComputeFABRIK()
{
  int maxTries = 100;
  int tryCount = 0;
  glm::vec3 anchor = m_joints[0]->position;
  while (glm::length(m_joints[m_joints.size() - 1]->position - m_target) > m_error && tryCount != maxTries)
  {
    ++tryCount;
    // forward solve
    ComputeForward(m_target);

    // backward solve
    ComputeBackward(anchor);
  }
  for (std::vector<Bone*>::iterator bone = m_bones.begin(); bone != m_bones.end(); ++bone)
  {
    (*bone)->ComputeModelTransforms();
  }
}

void Rig::ComputeForward(glm::vec3& target)
{
  m_joints[m_joints.size() - 1]->position = target;
  for (std::vector<Bone*>::reverse_iterator bone = m_bones.rbegin(); bone != m_bones.rend(); ++bone)
  {
    (*bone)->AdjustForward();
  }
}

void Rig::ComputeBackward(glm::vec3& target)
{
  m_joints[0]->position = target;
  for (std::vector<Bone*>::iterator bone = m_bones.begin(); bone != m_bones.end(); ++bone)
  {
    (*bone)->AdjustBackward();
  }
}

glm::vec3 Rig::GetRootPosition()
{
  return m_joints[0]->position;
}

void Rig::Draw(Renderer* renderer)
{
  for (std::vector<Bone*>::iterator bone = m_bones.begin(); bone != m_bones.end(); ++bone)
  {
    (*bone)->Draw(renderer);
  }
}
