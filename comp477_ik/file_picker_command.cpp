#include <string>
#include <iostream>
#include "file_picker_command.h"
#include "mouse_picker.h"
#include "mega_rig.h"



FilePickerCommand::FilePickerCommand(MegaRig** rig, MousePicker* mp) : m_megaRig(rig), m_mousePicker(mp)
{
}

FilePickerCommand::~FilePickerCommand()
{
}

void FilePickerCommand::execute()
{
  std::string fileName;
  std::cout << "Enter rig file name: ";
  std::cin >> fileName;
  m_mousePicker->Clear(); // dump all existing handles
  delete *m_megaRig;
  *m_megaRig = new MegaRig(m_mousePicker, fileName);
}
