#include <iostream>
#include <cmath>
#include <algorithm>
#include "ray_caster.h"
#include "cursor.h"
#include "camera.h"
#include "aabb.h"



RayCaster::RayCaster() : m_intersectables()
{
}


RayCaster::~RayCaster()
{
}

void RayCaster::RegisterBoundingBox(AABB* boundingBox)
{
  m_intersectables.push_back({ boundingBox });
}

AABB* RayCaster::CastMouseRay(Cursor * cursor, Camera * camera, float screenWidth, float screenHeight)
{
  return CastRay(camera->getPosition(), computeMouseRay(cursor, camera, screenWidth, screenHeight));
}

void RayCaster::Clear()
{
  m_intersectables.clear();
}

// TODO clean this up if time permits
AABB* RayCaster::CastRay(glm::vec3 from, glm::vec3 direction)
{
  float distance = 1000; // if further than 1000 no hit
  AABB* closest = NULL;
  for (std::vector<Intersectable>::iterator intersectable = m_intersectables.begin(); intersectable != m_intersectables.end(); ++intersectable)
  {
    glm::vec3 bMin = (*intersectable).boundingBox->GetMin();
    glm::vec3 bMax = (*intersectable).boundingBox->GetMax();

    float t0x = (bMin.x - from.x) / direction.x;
    float t1x = (bMax.x - from.x) / direction.x;
    float t0y = (bMin.y - from.y) / direction.y;
    float t1y = (bMax.y - from.y) / direction.y;
    float t0z = (bMin.z - from.z) / direction.z;
    float t1z = (bMax.z - from.z) / direction.z;

    if (t0x > t1x)
    {
      std::swap(t0x, t1x);
    }

    if (t0y > t1y)
    {
      std::swap(t0y, t1y);
    }

    if (t0z > t1z)
    {
      std::swap(t0z, t1z);
    }



    if (t0x > t1y || t0y > t1x)
    {
      continue;
    }
    float tMin = (t0x > t0y) ? t0x : t0y;
    float tMax = (t1x < t1y) ? t1x : t1y;
    if (tMin > t1z || t0z > tMax)
    {
      continue;
    }
    if (t0z > tMin)
    {
      tMin = t0z;
    }
    if (t1z < tMax)
    {
      tMax = t1z;
    }

    float shortest = fmin(tMin, tMax);
    if (shortest < distance)
    {
      distance = shortest;
      closest = intersectable->boundingBox;
    }
  }

  return closest;
}

glm::vec3 RayCaster::computeMouseRay(Cursor * cursor, Camera * camera, float screenWidth, float screenHeight)
{
  float x = (cursor->getX() * 2) / screenWidth - 1.0f;
  float y = 1.0f - (cursor->getY() * 2) / screenHeight;
  glm::vec4 rayClip = { x, y, -1.0f, 1.0f };
  glm::vec4 rayEye = glm::inverse(camera->getProjection()) * rayClip;
  rayEye = glm::vec4(rayEye.x, rayEye.y, -1.0f, 0.0f);
  rayEye = glm::inverse(camera->getView()) * rayEye;


  glm::vec3 rayWorld = glm::normalize(glm::vec3(rayEye.x, rayEye.y, rayEye.z));

  return rayWorld;
}
