#include "mouse_picker.h"
#include "handle.h"
#include "aabb.h"
#include "ray_caster.h"



MousePicker::MousePicker(Cursor * cursor, Camera * camera): m_cursor(cursor), m_camera(camera), m_rayCaster(new RayCaster())
{
}

MousePicker::~MousePicker()
{
}

Handle * MousePicker::CreateHandle(glm::vec3 position, glm::vec3 bounds, glm::vec3 color)
{
  Handle* handle = new Handle(m_cursor, position, bounds, color);
  RegisterHandle(handle);

  return handle;
}

void MousePicker::Clear()
{
  m_rayCaster->Clear();
  m_handleMap.clear();
}

void MousePicker::RegisterHandle(Handle * handle)
{
  m_rayCaster->RegisterBoundingBox(handle->GetBoundingBox());
  m_handleMap[handle->GetBoundingBox()] = handle;
}

void MousePicker::MouseDown()
{
  if (m_currentSelection)
  {
    m_currentSelection->Select();
  }
}

void MousePicker::MouseUp()
{
  if (m_currentSelection && m_currentSelection->Selected())
  {
    m_currentSelection->Release();
  }
}

void MousePicker::Update()
{
  if (m_currentSelection && m_currentSelection->Selected())
  {
    m_currentSelection->Update();
    return;
  }

  AABB* i = m_rayCaster->CastMouseRay(m_cursor, m_camera, 1280, 720);
  if (i)
  {
    Handle* handleOfIntersection = m_handleMap[i];
    if (m_currentSelection != handleOfIntersection)
    {
      // hover exit if currently hovering
      if (m_currentSelection)
      {
        m_currentSelection->HoverExit();
      }
      // hover enter if new target is entered
      if (handleOfIntersection)
      {
        handleOfIntersection->HoverEnter();
        m_currentSelection = handleOfIntersection;
      }
    }
  }
  else
  {
    if (m_currentSelection)
    {
      m_currentSelection->HoverExit();
      m_currentSelection = NULL;
    }
  }
}

// MOUSE PICKER COMMANDS
MousePickerClickCommand::MousePickerClickCommand(MousePicker * mp) : m_mousePicker(mp)
{
}

void MousePickerClickCommand::execute()
{
  m_mousePicker->MouseDown();
}

MousePickerReleaseCommand::MousePickerReleaseCommand(MousePicker * mp) : m_mousePicker(mp)
{
}

void MousePickerReleaseCommand::execute()
{
  m_mousePicker->MouseUp();
}
