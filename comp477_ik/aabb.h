#pragma once
#include <glm/glm.hpp>
#include "model.h"
class Shader;
class Renderer;

struct AABB
{
  glm::vec3 position;
  glm::vec3 color;
  float width;
  float height;
  float thickness;

  AABB();
  AABB(glm::vec3 position, float width, float height, float thickness, glm::vec3 color = { 0.8, 0.8, 0.8 });
  ~AABB();

  glm::vec3 GetMin();
  glm::vec3 GetMax();
  void Draw(Renderer* renderer);

  static void Init();
private:
  // static rendering resources
  static Model* MODEL;
  static Shader* SHADER;
  static BasicVertex VERTS[];
  static unsigned int INDICIES[];
};

